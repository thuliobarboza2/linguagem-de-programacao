<?php

$salario = 0.0;
echo "Digite o seu salário: ";
$salario = fgets(STDIN);
echo "\n";
if($salario < 1000){
    $salario = $salario + ($salario * 10/100); // Ajusta em 10%

} else{
    if ($salario > 2500){
        $salario = $salario * (1 + 3/100); // Ajusta em 3%
    } else{
        $salario = $salario * 1.05; // Ajusta em 5%
    }
}


echo "O novo salario é: ".number_format($salario, 2, '.')."\n";

?>
